from PIL import Image

def effaceEcran():
    """Efface l'écran de la console"""
    import os
    os.system("clear")

def affichageMenu():
    """Affiche les choix du menu"""
    print("-O- : Ourvir l'image(et l'afficher)")
    print("-A- : Afficher l'image chargée (la charger au besoin")
    print("-I- : Afficher les informations sur l'image")
    print("-N- : Mettre l'image en négatif")
    print("-C- : Effectuer la rotation des couleurs de l'image")
    print("-R- : Effectuer la rotation physique de l'image")
    print("-S- : Effectuer une transformation symétrique de l'image")
    print("-Q- : Quitter le programme")
    
def ouvrirImage():
    """Importer l'image et l'ouvrir"""
    print("Donner le chemin d'accès (absolu ou relatif) au fichier image : ")
    im = Image.open(input("> ")) 
    return im


def afficherInfosImage(im):
    """Afficher les infos de l'image importĂŠe"""
    print("La taille de l'image est :", im.size)
    print("Le format de l'image est :", im.format)
    print("Le mode de l'image est :", im.mode)
    print(" ")

def afficherImage(im):
    """Afficher l'image importĂŠe"""
    im.show()

def negatiCouleur(im):
    """Négatif des couleurs de l'image"""
    largeur,hauteur = im.size
    for ligne in range(0, hauteur):
        for colonne in range(0, largeur):
            R,G,B = im.getpixel((colonne,ligne))
            nouveauR = 255 - R
            nouveauG = 255 - G
            nouveauB = 255 - B
            im.putpixel((colonne,ligne),(nouveauR,nouveauG,nouveauB))
    im.show()

def decalageCouleur(im):
    """Décalage de couleurs"""
    choix2 = "0"#initialisation du choix 2
    while choix2!= "r":
        effaceEcran()
        print("-BRG-")
        print("-GBR-")
        print("-BGR-")
        print("-R- : Retour au menu principal")
        choix2 = input("Que voulez-vous faire ? ")
        if choix2 == "BRG":
            
            largeur,hauteur = im.size
            for ligne in range(0, hauteur):
                for colonne in range(0, largeur):
                    R,G,B = im.getpixel((colonne,ligne))
                    im.putpixel((colonne,ligne),(B,R,G))
            im.show()
            
        elif choix2 == "GBR":
            largeur,hauteur = im.size
            for ligne in range(0, hauteur):
                for colonne in range(0, largeur):
                    R,G,B = im.getpixel((colonne,ligne))
                    im.putpixel((colonne,ligne),(G,B,R))
            im.show()
            
        elif choix2 == "BGR":
            largeur,hauteur = im.size
            for ligne in range(0, hauteur):
                for colonne in range(0, largeur):
                    R,G,B = im.getpixel((colonne,ligne))
                    im.putpixel((colonne,ligne),(B,G,R))
            im.show()
            
        else:
            quitterProgramme()

def rotationImage(im):
    """Rotation de l'image : 90°, 180°, -90° """
    choix3 = "0"#initialisation du choix 3
    while choix3!= "r":
        effaceEcran()
        print("-Gauche-")
        print("-Droite-")
        print("-Retourner-")
        print("-R- : Retour au menu principal")
        choix3 = input("Que voulez-vous faire ? ")
        if choix3 == "Gauche":
            im.rotate(90).show()
        elif choix3 == "Droite":
            im.rotate(-90).show()
        elif choix3 == "Retourner":
            im.rotate(180).show()
        else:
            quitterProgramme

def symetrieImage(im):
    """symétrie axiale verticale ou horizontale de l'image"""
    pass

def quitterProgramme():
    """Met fin au programme"""
    pass


choix = "0"#initialisation du choix
while choix!= "q":
    effaceEcran()
    affichageMenu()
    choix = input("Que voulez-vous faire ? ")
    if choix == "O":
        im = ouvrirImage()
        afficherImage(im)
    elif choix == "A":
        afficherImage(im)
    elif choix == "I":
        afficherInfosImage(im)
    elif choix == "N":
        negatifCouleur(im)
    elif choix == "C":
        decalageCouleur(im)
    elif choix == "R":
        rotationImage(im)
    elif choix == "S":
        symetrieImage(im)
    elif choix == "Q":
        pass
    else:
        quitterProgramme
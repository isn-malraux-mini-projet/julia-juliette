def effaceEcran():
    """Efface l'écran de la console"""
    pass

def affichageMenu():
    """Affiche les choix du menu"""
    print("1 : Modifier la valeur de a")
    print("2 : Modifier la valeur de b")
    print("3 : Afficher la valeur de a+b")
    print("4 : Afficher la valeur de a-b")
    print("5 : Afficher la valur de a*b")
    print("6 : Afficher la valeur de a/b")
    print("q : Quitter le programme")


def affichageVariables(a,b):
    """Affiche les valeurs des deux variables a et b"""
    print("a =", a)
    print("b =", b)


def demandeValeurs(nomVariable, valeurInitiale):
    """Demande la nouvelle valeur pour une variable:
            - nommée nomVariable
            - ayant pour valeur initiale valeurInitiale
        Valeur de retour : - la nouvelle valeur si un nombre a été entré
                           - la valeur initiale sinon avec un message d'erreur"""
    nouvelleValeur = float(input("Nouvelle valeur de "+nomVariable+" : "))
    return nouvelleValeur

def affichageSomme(a,b):
    """Efface l'écran, affiche la valeur des deux variables et affiche la somme"""
    somme = a + b
    print(somme)
    
def affichageDifference(a,b):
    """Efface l'écran, affiche la valeur des deux variables et affiche la différence"""
    difference = a - b
    print(difference)

def affichageProduit(a,b):
    """Efface l'écran, affiche la valeur des deux variables et affiche le produit"""
    produit = a * b
    print(produit)

def affichageQuotient(a,b):
    """Efface l'écran, affiche la valeur des deux variables et affiche le quotient avec un
    message d'erreur en cas de division par 0"""
    quotient = a / b
    print(quotient)


a=0#initilisation des variables
b=0
affichageVariables(a,b)

choix = "0"#initialisation du choix
while choix!= "q":
    affichageMenu()
    choix = input("Choisir une action: ")
    if choix == "1":
        a = demandeValeurs("a",a)
        affichageVariables(a,b)
    elif choix == "2":
        b = demandeValeurs("b",b)
        affichageVariables(a,b)
    elif choix == "3":
        affichageSomme(a,b)
    elif choix == "4":
        affichageDifference(a,b)
    elif choix == "5":
        affichageProduit(a,b)
    elif choix == "6":
        affichageQuotient(a,b)
    elif choix.upper() == "Q":
        effaceEcran()
    else:
        affichageVariables
    